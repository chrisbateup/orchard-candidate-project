﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Orchard.Candidate.Net.Areas.Dashboard.Filters
{
    public class IdActionFilter : ActionFilterAttribute
    {
        public int id { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.ActionParameters["id"] = this.id;
            base.OnActionExecuting(filterContext);
        }
    }
}