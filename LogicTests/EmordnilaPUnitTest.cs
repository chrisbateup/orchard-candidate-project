﻿using System;
using LogicQuestions.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicTests
{
    [TestClass]
    public class EmordnilaPUnitTest
    {
        [TestMethod]
        public void SingleWordNotValid_ShouldBeFalse()
        {
            // Arrange

            // Act
            bool isValid = emordnilaP.IsPalindrome("rubbish");

            // Assert
            Assert.IsFalse(isValid);
        }

        [TestMethod]
        public void SimpleValidWord_ShouldBeTrue()
        {
            // Arrange

            // Act
            bool isValid = emordnilaP.IsPalindrome("TacoCat");

            // Assert
            Assert.IsTrue(isValid);
        }

        [TestMethod]
        public void ValidPhraseWithPunctuation_ShouldBeTrue()
        {
            // Arrange

            // Act
            bool isValid = emordnilaP.IsPalindrome("No lemon, no melon");

            // Assert
            Assert.IsTrue(isValid);
        }
    }
}
