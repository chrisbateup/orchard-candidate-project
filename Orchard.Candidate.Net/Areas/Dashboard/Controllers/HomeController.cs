﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Orchard.Candidate.Net.Areas.Dashboard.Filters;
using Orchard.Candidate.Net.Areas.Dashboard.Models;

namespace Orchard.Candidate.Net.Areas.Dashboard.Controllers
{
    [RouteArea("Dashboard")]
    [Authorize]
    public class HomeController : Controller
    {
        [IdActionFilter(id = 1)]
        [HttpGet]
        public async Task<ActionResult> Index(int? id)
        {
            // Handle initial request
            if (Request.IsAjaxRequest() == false)
            {
                var viewModel = new HomeViewModel
                {
                    Id = id,
                    Username = User.Identity.GetUserName()
                };
                return View(viewModel);
            }

            // Handle ajax requests
            // This code would be moved out of the controller.
            // Ideally we would setup a service and model for a post.
            // The service would then be used to return a list of posts.
            var strResponse = "";
            var uri = $"https://jsonplaceholder.typicode.com/user/{id}/posts";

            using (var http = new HttpClient())
            {
                strResponse = await http.GetStringAsync(uri);
            }

            return Json(strResponse, JsonRequestBehavior.AllowGet);
        }
    }
}