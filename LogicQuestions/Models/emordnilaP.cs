﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LogicQuestions.Models
{
    public class emordnilaP
    {
        public static bool IsPalindrome(string input)
        {
            input = input.ToLower();

            var rgx = new Regex("[^a-z]", RegexOptions.Multiline);
            string tidiedInput = rgx.Replace(input, "");

            char[] array = tidiedInput.ToCharArray();

            var reversedArray = new char[array.Length];
            Array.Copy(array, reversedArray, array.Length);
            Array.Reverse(reversedArray);

            return (array.SequenceEqual(reversedArray));
        }
    }
}