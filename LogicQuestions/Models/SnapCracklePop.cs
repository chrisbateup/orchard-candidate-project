﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace LogicQuestions.Models
{
    public class SnapCracklePop
    {
        public static string Run()
        {
            StringBuilder sb = new StringBuilder();

            for (var i = 1; i < 101; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                    sb.Append("POP");
                else if (i % 3 == 0)
                    sb.Append("SNAP");
                else if (i % 5 == 0)
                    sb.Append("CRACKLE");
                else
                    sb.Append(i.ToString());
            }

            return sb.ToString();
        }
    }
}