﻿using System;
using LogicQuestions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LogicTests
{
    [TestClass]
    public class InstagramUnitTest
    {
        [TestMethod]
        public void WhenHetero_ReturnHeterogram()
        {
            string gram = Instagram.Check("uncopyrightable");
            Assert.AreEqual(gram, "HETEROGRAM");
        }

        [TestMethod]
        public void WhenIsogram_ReturnIsogram()
        {
            string gram = Instagram.Check("Caucasus");
            Assert.AreEqual(gram, "ISOGRAM");
        }

        [TestMethod]
        public void WhenNotagram_ReturnNotagram()
        {
            string gram = Instagram.Check("authorising");
            Assert.AreEqual(gram, "NOTAGRAM");
        }
    }
}
