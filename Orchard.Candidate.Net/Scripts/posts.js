﻿var posts = function() {
    var _postsPerPage = 5;
    var _newestPostIndex = -1;
    var _userId = 0;

    function updatePosts(userId) {
        _userId = userId;
        hidePosts();
        showLoader();
        setTimeout(showNextPosts, 3000);
    }

    function hidePosts() {
        $("#posts-container ul").empty("li");
    }

    function showLoader() {
        $("#posts-container .loader").show();
    }

    function hideLoader() {
        $("#posts-container .loader").hide();
    }

    function showNextPosts() {
        hideLoader();
        loadPosts();
    }

    function loadPosts() {
        $.getJSON("/Dashboard/Home/",
            function (result) {
                var data = $.parseJSON(result);
                
                // Determine the newest post to show (assuming highest index == newest)
                if (_newestPostIndex - _postsPerPage >= 0)
                    _newestPostIndex = _newestPostIndex - _postsPerPage;
                else
                    _newestPostIndex = data.length;

                var items = [];
                $.each(data,
                    function(key, val) {
                        items.push(`<li><strong>${val.title}</strong><br />${val.body}</li>`);
                    });

                // Add the next page of results to the list
                $((items)
                    .slice(_newestPostIndex - _postsPerPage, _newestPostIndex)
                        .reverse()
                        .join(""))
                    .appendTo("#posts-container ul");

                if (_newestPostIndex - _postsPerPage >= 0)
                    $(".js-update-posts").text("Older Posts");
                else {
                    $("#posts-container ul").append("<li>No more posts</li>");
                    $(".js-update-posts").text("Most Recent");
                }
            });
    }

    return {
        updatePosts: updatePosts
    };
}();
