﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orchard.Candidate.Net.Areas.Dashboard.Models
{
    public class HomeViewModel
    {
        public int? Id { get; set; }
        public string Username { get; set; }
    }
}