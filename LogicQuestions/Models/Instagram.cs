﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LogicQuestions
{
    public class Instagram
    {
        public static string Check(string input)
        {
            bool checkHeterogram = true;
            bool checkIsogram = true;

            input = input.ToLower();

            var rgx = new Regex("[^a-z]", RegexOptions.Multiline);
            input = rgx.Replace(input, "");

            var checkedLetterCount = 0;
            var letterCounts = new int[input.Length];

            for (var i = 0; i < letterCounts.Length; i++)
            {
                letterCounts[i] = 0;

                for (var j = 0; j < letterCounts.Length; j++)
                {
                    if (input[j] == input[i])
                        letterCounts[i]++;
                }

                if (i == 0)
                    checkedLetterCount = letterCounts[i];

                if (checkHeterogram && letterCounts[i] > 1)
                    checkHeterogram = false;
                else if (checkIsogram && letterCounts[i] != checkedLetterCount)
                    checkIsogram = false;

                if (checkHeterogram == false && checkIsogram == false)
                    break;
            }

            if (checkHeterogram)
                return "HETEROGRAM";

            if (checkIsogram)
                return "ISOGRAM";

            return "NOTAGRAM";
        }
    }
}